# README #

Have vagrant and oracle virtualbox installed (google and download if you don't).

Create a directory in your user folder. (On Windows, C:/Users/YourUsername/vagrant)

Pull this repo into the directory you just created. I recommend SourceTree that is promoted on this site, BitBucket for managing these files. https://www.sourcetreeapp.com/

View the VagrantFile you just pulled from the repo and retrieve the ip set after the command: config.vm.network "private_network", ip:

Set that ip in your Windows host file. See the host.txt file in the repo for different domains and subdomains to set that ip to.

In Windows, click on the Start Button and type in text field (but don't click on the program or hit enter yet): cmd

cmd.exe should appear in the program list. Right click on it and select 'Run as Administrator'.

cd to your director (eg. cd C:/Users/YourUsername/vagrant

Type: vagrant up and hit enter

You can ssh into vagrant using the ip from above with username: vagrant and password: vagrant