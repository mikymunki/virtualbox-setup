#!/usr/bin/env bash

sudo echo 'export PATH=$PATH:/sbin:/usr/sbin' >> ~/.profile
sudo mkdir -p /web/sites/hustler/mas_content_ro
sudo mkdir -p /web/sites/hustler/model_bios
sudo mkdir -p /web/sites/hustler/screenshots
sudo mkdir -p /web/sites/hustler/videos
sudo mkdir -p /web/sites/hustler/cms.lfpcontent.com/cms/content/banners/tours
sudo mkdir -p /web/sites/hustler/cms.lfpcontent.com/cms/content/groups
sudo mkdir -p /web/sites/hustler/cms.lfpcontent.com/cms/content/magazines
sudo mkdir -p /web/sites/hustler/cms.lfpcontent.com/cms/content/performers
sudo rm -rf /var/www # We don't need this because
#sudo ln -fs /vagrant /var/www # we will just link to the vboxsf synced folder.
#sudo ln -s /vagrant /web/sites/hustler # Needed because lfpcms depends on absolute paths starting with /web/sites/.

cat > /etc/apt/sources.list.d/dotdeb.list << EOF
deb http://packages.dotdeb.org wheezy-php56 all
deb-src http://packages.dotdeb.org wheezy-php56 all
EOF

wget http://www.dotdeb.org/dotdeb.gpg -O- | apt-key add -

sudo apt-get update
sudo apt-get install -y vim build-essential git-core curl 
sudo apt-get -y install php5 php5-mhash php5-mcrypt php5-curl php5-cli php5-gd php5-intl php5-xdebug php5-mysql

sudo echo "suhosin.executor.include.whitelist = phar" >> /etc/php5/cli/php.ini
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

#MySQL root password is stored in /etc/mysql/debian.cnf
# to remove mysql if no room in /var/lib/mysql
# http://askubuntu.com/questions/172514/how-do-i-uninstall-mysql
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get install -y mysql-server

#sudo service mysql restart
#sudo chmod 777 '/var/run/mysqld/mysqld.sock

sudo apt-get install -y vim
sudo apt-get install -y libapache2-mod-auth-mysql 
sudo apt-get install -y libapache2-mod-php5 
sudo apt-get install -y apache2 #memcached #php5-memcache
# stub the Memcache class instead: github.com/schmittjoh/php-stubs/tree/master/res/php/memcache
sudo a2enmod rewrite auth_mysql
#echo 'SetEnv ENVIRONMENT DEVELOPMENT' >> /etc/apache2/apache2.conf
echo 'ServerName localhost' >> /etc/apache2/apache2.conf

# Enable error reporting in PHP.
sudo sed -i 's/error_reporting =/; error_reporting =/' /etc/php5/apache2/php.ini
sudo sed -i '/; error_reporting =/a error_reporting = E_ALL' /etc/php5/apache2/php.ini
sudo sed -i 's/display_errors = Off/display_errors = On/' /etc/php5/apache2/php.ini
sudo sed -i 's/auto_append_file =/auto_append_file = \/web\/sites\/hustler_cms\/hustler-cms\/auto_append.php/' /etc/php5/apache2/php.ini
sudo sed -i 's/display_startup_errors = Off/display_startup_errors = On/' /etc/php5/apache2/php.ini
#sudo sed -i 's/disable_functions =/disable_functions = error_reporting, ini_set/' /etc/php5/apache2/php.ini
sudo sed -i 's/short_open_tag = Off/short_open_tag = On/' /etc/php5/apache2/php.ini
sudo sed -i 's/;date.timezone =/date.timezone = America\/Los_Angeles/' /etc/php5/apache2/php.ini
sudo sed -i 's/;date.timezone =/date.timezone = America\/Los_Angeles/' /etc/php5/cli/php.ini
sudo sed -i 's/"syntax on/syntax on/' /etc/vim/vimrc
sudo sed -i 's/"set background=dark/set background=dark/' /etc/vim/vimrc

cat > /root/bashrc_tmp << EOF

	alias dev='cd /web/sites/hustler_cms/hustler-cms'
	alias cms='cd /web/sites/hustler_cms/cms.lfpcontent.com'
	
	# search function
	# usage: sgrep [-i] "search token"
	function sgrep {
		FILE_LIST_NO_BINARIES="-Il"
			DEFAULT_OPTS_WITH_FIND="-hn"
			opts=\$DEFAULT_OPTS_WITH_FIND

			SKIP_EXTS_RAW='png jpg gif GIF JPG JPEG PNG jpeg db svn svn-base psd swp swf doc pdf zip txt bmp bak xls PDF icns ppt tmp'
			SKIP_DIRS=".*/.svn\|.*/images\|.*/templates_c"
			SKIP_EXTS="`echo \$SKIP_EXTS_RAW | sed -e 's_\(\w*\)_.*\.\1_g' | sed -e 's_ _\\\|_g'`"

			if [[ \$# -eq 1 ]]
				then
					iopt=''
					searchterm=\$1
					echo "Case sensitive search for: '\$searchterm'"
					elif [ \$# -eq 2 ]  &&  [ "\$1" == "-i" ]
					then
					iopt=\$1
					searchterm=\$2
					echo "Case insensitive search for: '\$searchterm'"
			else
				echo "Incorrect usage."
				echo "Usage: sgrep [-i] \"search token\""
				return
				fi

				find .  \
				-regex "\$SKIP_DIRS" -prune , \
				-not -regex "\$SKIP_EXTS" \
				-type f -print0 \
				| xargs -0 grep \$iopt \$FILE_LIST_NO_BINARIES "\$searchterm" \
				| while read f; do echo -e "\n\e[00;32m \$f: \e[00m"; grep \$iopt --color=always \$opts "\$searchterm" "\$f"; done
	}
EOF

cat > /etc/apache2/sites-available/hustler.com << EOF
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName local.hustler.com
	ServerAlias local.tour.hustler.com
	ServerAlias local.members.hustler.com
	ServerAlias YOURUSERNAME.hustler.com
	ServerAlias local.barelylegal.com
	ServerAlias local.tour.barelylegal.com
	ServerAlias local.members.barelylegal.com
	ServerAlias local.m.hustler.com
	ServerAlias local.m.tour.hustler.com
	DocumentRoot /web/sites/hustler_cms/hustler-cms
	<Directory />
		   Options FollowSymLinks
		   AllowOverride All
	</Directory>
	<Directory /web/sites/hustler_cms>
		   Options Indexes FollowSymLinks MultiViews
		   AllowOverride All
		   Order allow,deny
		   allow from all
	</Directory>
	ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
	<Directory "/usr/lib/cgi-bin">
		   AllowOverride All
		   Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
		   Order allow,deny
		   Allow from all
	</Directory>
	ErrorLog ${APACHE_LOG_DIR}/error.log
	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

cat > /etc/apache2/sites-available/m.hustler.com << EOF
<VirtualHost *:80>
       ServerAdmin webmaster@localhost
       ServerAlias local.tablet.hustler.com
       ServerAlias local.tablet.tour.hustler.com
       ServerAlias local.tablet.members.hustler.com
       ServerAlias local.m.members.hustler.com
       DocumentRoot /web/sites/hustler_cms/hustler-cms/mobile
       <Directory />
               Options FollowSymLinks
               AllowOverride All
       </Directory>
       <Directory /web/sites/hustler_cms>
               Options Indexes FollowSymLinks MultiViews
               AllowOverride All
               Order allow,deny
               allow from all
       </Directory>
       ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
       <Directory "/usr/lib/cgi-bin">
               AllowOverride All
               Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
               Order allow,deny
               Allow from all
       </Directory>
       ErrorLog ${APACHE_LOG_DIR}/error.log
       # Possible values include: debug, info, notice, warn, error, crit,
       # alert, emerg.
       LogLevel warn
       CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

cat > /etc/apache2/sites-available/cms.lfpcontent.com << EOF
<VirtualHost *:80>
   ServerAdmin webmaster@localhost
   ServerName local.cms.lfpcontent.com
   DocumentRoot /web/sites/hustler_cms/cms.lfpcontent.com/
   <Directory />
       Options FollowSymLinks
       AllowOverride All
   </Directory>
   <Directory /web/sites/hustler_cms>
       Options Indexes FollowSymLinks MultiViews
       AllowOverride All
       Order allow,deny
       allow from all
   </Directory>
   ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
   <Directory "/usr/lib/cgi-bin">
       AllowOverride All
       Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
       Order allow,deny
       Allow from all
   </Directory>
   ErrorLog ${APACHE_LOG_DIR}/error.log
   # Possible values include: debug, info, notice, warn, error, crit,
   # alert, emerg.
   LogLevel warn
   CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

cat > /etc/apache2/sites-available/mvc.hustler.com << EOF
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName mvc.hustler.com
	ServerAlias mvc.tour.hustler.com
	ServerAlias mvc.members.hustler.com
	ServerAlias YOURUSERNAME.hustler.com
	ServerAlias mvc.barelylegal.com
	ServerAlias mvc.tour.barelylegal.com
	ServerAlias mvc.members.barelylegal.com
	ServerAlias mvc.m.hustler.com
	ServerAlias mvc.m.tour.hustler.com
	DocumentRoot /web/sites/hustler_cms/hustler-cms
	<Directory />
		   Options FollowSymLinks
		   AllowOverride All
	</Directory>
	<Directory /web/sites/hustler_cms>
		   Options Indexes FollowSymLinks MultiViews
		   AllowOverride All
		   Order allow,deny
		   allow from all
	</Directory>
	ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
	<Directory "/usr/lib/cgi-bin">
		   AllowOverride All
		   Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
		   Order allow,deny
		   Allow from all
	</Directory>
	ErrorLog ${APACHE_LOG_DIR}/error.log
	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF


#sudo chown vagrant:vagrant /web/sites/.htaccess

sudo ln -s /etc/apache2/sites-available/cms.lfpcontent.com  /etc/apache2/sites-enabled/cms.lfpcontent.conf
sudo ln -s /etc/apache2/sites-available/hustler.com  /etc/apache2/sites-enabled/hustler.conf
sudo ln -s /etc/apache2/sites-available/m.hustler.com  /etc/apache2/sites-enabled/m.hustler.conf
sudo ln -s /etc/apache2/sites-available/mvc.hustler.com  /etc/apache2/sites-enabled/mvc.hustler.conf

sudo ln -s /etc/apache2/mods-available/php5.load /etc/apache2/mods-enabled/php5.load
sudo ln -s /etc/apache2/mods-available/php5.conf /etc/apache2/mods-enabled/php5.conf

sudo chown -R vagrant:vagrant /web/sites/hustler
sudo chmod -R 777 /web/sites/hustler
sudo mkdir /web/sites/hustler_cms
sudo chown vagrant:vagrant /web/sites/hustler_cms

#https://github.com/matthewbdaly/vagrant-php-dev-boilerplate/blob/master/bootstrap.sh
#Configure XDebug
cat > /etc/php5/apache2/conf.d/20-xdebug.ini << EOF
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_mode=req
;xdebug.remote_host=127.0.0.1
xdebug.remote_port=9000
xdebug.remote_log=/var/log/xdebug.log
xdebug.remote_connect_back=1
xdebug.idekey=netbeans-xdebug
EOF

cat > /etc/php5/cli/conf.d/20-xdebug.ini << EOF
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_mode=req
;xdebug.remote_host=127.0.0.1
xdebug.remote_port=9000
xdebug.remote_log=/var/log/xdebug.log
xdebug.remote_connect_back=1
xdebug.idekey=netbeans-xdebug
EOF

cat > /root/.vimrc << EOF
set number
EOF

#install node
curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo apt-get -y autoremove

sudo apt-get update

sudo rm /etc/apache2/sites-enabled/0000-default

sudo service apache2 restart

# VBoxManage setextradata lfp_debian_squeeze_64
# VBoxInternal2/SharedFoldersEnableSymlinksCreate/cms 1

echo "Provisioning done."
